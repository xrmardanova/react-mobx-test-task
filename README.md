# React MobX Typescript Task Project with Spotify Services

## Setup

```
$ npm install
```

## Running

```
$ npm start
```

## Build

```
$ npm run build
```

## Test

```
$ npm test
```


## Code Format

```
$ npm run prettier
```

## Screens 

Login page
![](./screenshots/login.png)

Categories page 
![](./screenshots/categories.png)

Playlists page
![](./screenshots/playlists.png)

# License

MIT
