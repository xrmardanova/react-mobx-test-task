import * as React from 'react';
import { Card, CardMedia, Grid, CardActionArea } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import { StatusTypes } from 'app/constants';
import LoadingBar from 'app/components/LoadingBar';

import * as style from './style.css';

interface CategoriesProps {
  categories: Array<object>;
  status: any;
}

export class Categories extends React.Component<CategoriesProps> {
  constructor(props?: CategoriesProps, context?: any) {
    super(props, context);
  }
  render() {
    const { categories, status } = this.props;
    return (
      <Grid container spacing={2} direction="row">
        {status === StatusTypes.PENDING && <LoadingBar />}
        {categories.map((item: any) => (
          <Grid item xs={6} md={2} key={item.id}>
            <Card className={style.card}>
              <NavLink to={`/playlist/${item.id}`}>
                <CardActionArea>
                  <CardMedia
                    className={style.img}
                    image={
                      item.icons && item.icons.length > 0 && item.icons[0].url
                    }
                    title={item.name}
                  >
                    <span className={style.actions}>{item.name}</span>
                  </CardMedia>
                </CardActionArea>
              </NavLink>
            </Card>
          </Grid>
        ))}
      </Grid>
    );
  }
}
export default Categories;
