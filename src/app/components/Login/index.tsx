import * as React from 'react';
import {
  Container,
  Button,
  TextField,
  Typography,
} from '@material-ui/core';

import * as style from './style.css';

export interface LoginProps {
  login: (username: string, password: string) => any;
}
export interface LoginState {
  /* empty */
}

export class Login extends React.Component<LoginProps, LoginState> {
  constructor(props?: LoginProps, context?: any) {
    super(props, context);
    this.state = { editing: false };
  }

  private handleSubmit = (event: any) => {
    event.preventDefault();
    this.props.login(event.target.email.value, event.target.password.value);
  };
  render() {
    return (
      <Container component="main" maxWidth="xs" fixed>
        <div className={style.paper}>
          <div className={style.loginHeader}>
            <img src="assets/img/spotify-logo.png" />
            <Typography component="h5" variant="h5">
              To continue, log in to Spotify.
            </Typography>
          </div>
          <form
            className={style.form}
            onSubmit={(event) => this.handleSubmit(event)}
          >
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              color="primary"
              defaultValue="6d784b45be29461aa1d64642fb0a449c"
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="password"
              label="Password"
              name="password"
              type="password"
              autoComplete="current-password"
              autoFocus
              color="primary"
              defaultValue="e22be4db0aee4f82b11bb09d68f57711"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={style.submit}
              size="large"
            >
              Sign In
            </Button>
          </form>
        </div>
      </Container>
    );
  }
}

export default Login;
