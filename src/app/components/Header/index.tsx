import * as React from 'react';
import { Box } from '@material-ui/core';
import * as style from './style.css';
export interface HeaderProps {
  title?: any;
}

export interface HeaderState {
  /* empty */
}

export class Header extends React.Component<HeaderProps, HeaderState> {
  constructor(props?: HeaderProps, context?: any) {
    super(props, context);
  }
  render() {
    return (
      <Box className={style.header}>
        <div className={style.title}>
          <h1 className={style.titleText}>{this.props.title}</h1>
          <hr className={style.hr}></hr>
        </div>
      </Box>
    );
  }
}

export default Header;
