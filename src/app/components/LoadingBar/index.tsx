import * as React from 'react';
import { CircularProgress, Box } from '@material-ui/core';

import * as style from './style.css';

const LoadingBar = () => {
  return (
    <Box display="flex" className={style.loadingBar}>
      <CircularProgress color="primary" />
    </Box>
  );
};

export default LoadingBar;
