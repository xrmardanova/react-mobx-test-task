import * as React from 'react';
import { NavLink } from 'react-router-dom';
import {
  Box,
  ListItem,
  List,
  Hidden,
  Divider,
  Drawer,
  AppBar,
  Toolbar,
  IconButton,
  Badge,
  Avatar,
} from '@material-ui/core';
import AlbumRounded from '@material-ui/icons/AlbumRounded';
import MenuRounded from '@material-ui/icons/MenuRounded';
import { Theme, withStyles, createStyles } from '@material-ui/core/styles';
import * as style from './style.css';

const StyledBadge = withStyles((theme: Theme) =>
  createStyles({
    badge: {
      backgroundColor: '#44b700',
      color: '#44b700',
      boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
      '&::after': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        borderRadius: '50%',
        animation: '$ripple 1.2s infinite ease-in-out',
        content: '""',
      },
    },
    '@keyframes ripple': {
      '0%': {
        transform: 'scale(.8)',
        opacity: 1,
      },
      '100%': {
        transform: 'scale(2.4)',
        opacity: 0,
      },
    },
  })
)(Badge);

interface NavbarProps {
  container?: any;
  classes?: any;
}

export class Navbar extends React.Component<
  NavbarProps,
  { mobileOpen: boolean }
> {
  constructor(props) {
    super(props);
    this.state = { mobileOpen: false };
  }

  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };
  render() {
    const drawer = (
      <>
        <List>
          <Box className={style.userInfo}>
            <StyledBadge
              overlap="circle"
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              variant="dot"
            >
              <Avatar
                alt="David Down"
                src="assets/img/user.jpeg"
              />
            </StyledBadge>
            David Down
          </Box>

          <Divider />
          <NavLink
            to="/"
            activeClassName={style.navItemActive}
            className={style.navItem}
            exact
          >
            <ListItem button>
              <AlbumRounded />
              Categories
            </ListItem>
          </NavLink>
        </List>
      </>
    );

    return (
      <React.Fragment>
        <AppBar position="fixed" className={style.black}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={this.handleDrawerToggle}
            >
              <MenuRounded />
            </IconButton>
            <img src="assets/img/logo.svg" className={style.logo} />
          </Toolbar>
        </AppBar>
        <nav aria-label="mailbox folders">
          <Hidden smUp implementation="css">
            <Drawer
              variant="temporary"
              anchor="left"
              open={this.state.mobileOpen}
              onClose={this.handleDrawerToggle}
              ModalProps={{
                keepMounted: true,
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer variant="permanent" open>
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
      </React.Fragment>
    );
  }
}

export default Navbar;
