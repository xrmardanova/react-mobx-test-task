import * as React from 'react';
import {
  Card,
  Grid,
  CardActionArea,
  CardContent,
  Typography,
} from '@material-ui/core';
import { StatusTypes } from 'app/constants';
import LoadingBar from 'app/components/LoadingBar';

import * as style from './style.css';

interface PlaylistProps {
  playlists: Array<object>;
  status: any;
}

export class Playlist extends React.Component<PlaylistProps> {
  constructor(props?: PlaylistProps, context?: any) {
    super(props, context);
  }
  render() {
    const { playlists, status } = this.props;
    return (
      <Grid container spacing={2} direction="row">
        {status === StatusTypes.PENDING && <LoadingBar />}
        {playlists.map((item: any, index) => (
          <Grid item xs={12} md={2} key={item.id}>
            <Card>
              <CardActionArea>
                <img
                  src={
                    item.images && item.images.length > 0 && item.images[0].url
                  }
                  alt={item.name}
                  className={style.img}
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {item.name}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {item.description}
                  </Typography>
                  <Typography component="p">
                    {item.tracks.total} TRACKS
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        ))}
      </Grid>
    );
  }
}

export default Playlist;
