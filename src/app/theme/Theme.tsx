import * as React from 'react';
import {
  createMuiTheme,
  CssBaseline,
  MuiThemeProvider,
  responsiveFontSizes,
  Box,
} from '@material-ui/core';

const drawerWidth = 180;

const circular: any = {
  fontFamily: 'Circular',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 500,
  src: `
    local('Circular'),
    local('Circular-Book'),
    url('assets/fonts/circular-book.woff2') format('woff2')
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

const circularBold: any = {
  fontFamily: 'Circular-Bold',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 500,
  src: `
    local('Circular-Bold'),
    local('Circular-Bold'),
    url('assets/fonts/circular-black.woff2') format('woff2')
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
};

let theme = createMuiTheme({
  palette: {
    type: 'dark',
    background: {
      default: '#000',
      paper: '#fff',
    },
    primary: {
      main: '#04d95f',
      contrastText: '#fff',
    },
  },
  typography: {
    fontFamily: 'Circular',
  },
  spacing: 4,
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [circular, circularBold],
      },
    },
    MuiList: {
      root: {
        width: drawerWidth,
      },
    },
    MuiPaper: {
      root: {
        backgroundColor: '#000',
      },
    },
    MuiDrawer: {
      paper: {
        backgroundColor: '#101010',
        color: '#fff',
        fontWeight: 500,
      },
    },
    MuiAvatar: {
      root: {
        width: '28px',
        height: '28px',
      },
    },
    MuiDivider: {
      root: {
        backgroundColor: 'rgba(255, 255, 255, 0.12)',
      },
    },
    MuiBadge: {
      root: {
        marginRight: '6px',
      },
    },
    MuiFormLabel: {
      root: {
        color: '#fff',
      },
    },
    MuiInputBase: { root: { color: '#fff' } },
    MuiFormControlLabel: {
      root: {
        color: '#fff',
      },
    },
    MuiTouchRipple: {
      root: {
        '&:hover': {
          backgroundColor: 'green',
        },
        '&:focus': {
          backgroundColor: 'green',
        },
        '&:active': {
          backgroundColor: 'green',
        },
      },
      child: {
        backgroundColor: '#04d95f',
      },
    },
    MuiCardActionArea: {
      focusHighlight: {
        backgroundColor: '#04d95f',
      },
    },
  },
});

theme = responsiveFontSizes(theme);

const Theme = (props) => {
  const { children } = props;
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <Box display="flex">{children}</Box>
    </MuiThemeProvider>
  );
};

export default Theme;
