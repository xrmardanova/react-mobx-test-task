import Theme from './Theme';
import * as React from 'react';
import { createRender } from '@material-ui/core/test-utils';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('<Theme />', () => {
  let render;

  beforeAll(() => {
    render = createRender();
  });

  it('should work', () => {
    const wrapper = render(<Theme />);
    expect(wrapper).not.toEqual(null)
  });
});
