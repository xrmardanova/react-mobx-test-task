import * as React from 'react';
import { Route, Redirect } from 'react-router';

const ProtectedRoute = ({ render, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      localStorage.getItem('token') ? ( //eslint-disable-line no-undef
        render(props)
      ) : (
        <Redirect to="/login" />
      )
    }
  />
);

export default ProtectedRoute;
