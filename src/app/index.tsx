import React from 'react';
import { hot } from 'react-hot-loader/root';
import { Router, Route, Switch } from 'react-router';
import { CategoriesContainer } from 'app/containers/CategoriesContainer';
import { PlaylistsContainer } from 'app/containers/PlaylistsContainer';
import { LoginContainer } from 'app/containers/LoginContainer';
import { ProtectedRoute } from './helpers';

export const App = hot(({ history }) => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/login" component={LoginContainer} exact />
        <ProtectedRoute path="/" render={() => <CategoriesContainer />} exact />
        <Route
          path="/playlist/:id"
          component={PlaylistsContainer}
          exact
        />
      </Switch>
    </Router>
  );
});
