import * as React from 'react';
import { observer } from 'mobx-react';
import { useHistory } from 'react-router';
import { usePlaylistStore } from 'app/stores/PlaylistStore';
import Theme from 'app/theme';
import {Header, Navbar,Playlist} from 'app/components';

import * as style from './style.css';

export const PlaylistsContainer = observer((props) => {
  const history = useHistory();
  const playlistStore = usePlaylistStore(history);

  React.useEffect(() => {
    playlistStore.getPlaylists(props.match.params.id);
  }, [playlistStore.state]);

  return (
    <Theme>
      <Navbar />
      <div className={style.content}>
        <Header title="Playlists" />
        <Playlist
          playlists={playlistStore.activePlaylists}
          status={playlistStore.currentStatus}
        />
      </div>
    </Theme>
  );
});
