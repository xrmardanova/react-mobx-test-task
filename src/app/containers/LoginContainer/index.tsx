import * as React from 'react';
import { observer } from 'mobx-react';
import { useHistory } from 'react-router';
import { Login } from 'app/components';
import Theme from 'app/theme';
import { useUserStore } from 'app/stores/UserStore';

export const LoginContainer = observer(() => {
  const history = useHistory();

  const userStore = useUserStore(history);

  React.useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      localStorage.removeItem('token');
    }
  });
  return (
    <Theme>
      <Login login={userStore.login} />
    </Theme>
  );
});
