import * as React from 'react';
import { observer } from 'mobx-react';
import { useHistory } from 'react-router';
import { useCategoryStore } from 'app/stores/CategoryStore';
import Theme from 'app/theme';
import { Header, Navbar, Categories } from 'app/components';

import * as style from './style.css';

export const CategoriesContainer = observer(() => {
  const history = useHistory();
  const categoryStore = useCategoryStore(history);

  React.useEffect(() => {
    categoryStore.getCategories();
  }, [categoryStore.state]);

  return (
    <Theme>
      <Navbar />
      <div className={style.content}>
        <Header title="Categories" />
        <Categories
          categories={categoryStore.activeCategories}
          status={categoryStore.currentStatus}
        />
      </div>
    </Theme>
  );
});
