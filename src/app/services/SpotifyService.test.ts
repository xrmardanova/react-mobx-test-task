import SpotifyService from './SpotifyService';
import axios from 'axios';
import CategoryList from '../../../__mockapi__/GET/api-category.json';
import Playlist from '../../../__mockapi__/GET/api-playlist.json';

jest.mock('axios', () => {
  return {
    get: jest.fn(),
    post: jest.fn(),
  };
});

const mockedAxios = axios as jest.Mocked<typeof axios>;

const test_token = 'test_token';

const spotifyService = new SpotifyService();

describe('SpotifyService', () => {
  describe('getCategories', () => {
    it('should returns success with data', async () => {
      const data = CategoryList;

      mockedAxios.get.mockResolvedValue({ data });

      var response: any = await spotifyService.getCategories(test_token);

      expect(mockedAxios.get).toHaveBeenCalled();
      expect(mockedAxios.get).toHaveBeenCalledTimes(1);

      expect(typeof { value: response }).toBe('object');
      expect(response.categories.items.length).toBeGreaterThan(0);
      expect(response.categories.items.length).toEqual(
        response.categories.total
      );
    });
  });
  describe('getPlaylists', () => {
    it('should returns success with data', async () => {
      const data = Playlist;
      const categoryId = 'toplists';
      mockedAxios.get.mockResolvedValue({ data });

      var response: any = await spotifyService.getPlaylists(
        test_token,
        categoryId
      );

      expect(response.playlists.items.length).toBeGreaterThan(0);
      expect(response.playlists.items.length).toEqual(response.playlists.total);
    });
  });

  describe('login', () => {
    it('should returns success with token', async () => {
      const username = 'test';
      const password = 'test';
      const data = {
        access_token:
          'BQCSGYuMTD2Ug343auzR7OaKFOI8HBLQ80OigpSzdaPNs1icGxnJvZgjlUsNB3BqNHipakoUtQ4GVHUJWHU',
        token_type: 'Bearer',
        expires_in: 3600,
        scope: '',
      };
      mockedAxios.post.mockResolvedValue({ data });

      var response: any = await spotifyService.login(username, password);

      expect(response.access_token).not.toEqual(null);
    });
  });
});
