import axios from 'axios';
const queryString = require('query-string');
import { urlConfig } from '../config';

export default class SpotifyService {
  getCategories(token: string) {
    return new Promise((resolve, reject) => {
      axios
        .get(`${urlConfig.baseUrl('categories?limit=50&offset=0')}`, {
          headers: {
            Authorization: token,
          },
        })
        .then((response: any) => {
          resolve(response.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  getPlaylists(token: string, categoryId: string) {
    return new Promise((resolve, reject) => {
      axios
        .get(
          `${urlConfig.baseUrl(
            `categories/${categoryId}/playlists?limit=50&offset=0`
          )}`,
          {
            headers: {
              Authorization: token,
            },
          }
        )
        .then((response: any) => {
          resolve(response.data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  login(username: string, password: string) {
    return new Promise((resolve, reject) => {
      const headers = {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        auth: {
          username,
          password,
        },
      };
      const data = {
        grant_type: 'client_credentials',
      };

      axios
        .post(
          `${urlConfig.accountUrl('token')}`,
          queryString.stringify(data),
          headers
        )
        .then((response) => resolve(response.data))
        .catch((error) => reject(error.error));
    });
  }
}
