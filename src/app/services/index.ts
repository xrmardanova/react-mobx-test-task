import Spotify from './SpotifyService';

const Services = {
  SpotifyService: () => {
    return new Spotify();
  },
};

export default Services;
