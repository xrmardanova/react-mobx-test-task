export default {
  baseUrl: (url) => {
    return `https://api.spotify.com/v1/browse/${url}`;
  },
  accountUrl: (url) => {
    return `https://accounts.spotify.com/api/${url}`;
  },
};
