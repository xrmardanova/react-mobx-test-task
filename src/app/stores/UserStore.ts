import { useLocalStore } from 'mobx-react';
import { runInAction } from 'mobx';
import services from '../services';
import { StatusTypes } from 'app/constants';

export type UserStore = ReturnType<typeof useUserStore>;

export const useUserStore = (history: any) => {
  const store = useLocalStore(() => ({
    tokenInfo: null,
    state: StatusTypes.PENDING,

    login(username: string, password: string) {
      services
        .SpotifyService()
        .login(username, password)
        .then((response: any) => {
          localStorage.setItem(
            'token',
            `${response.token_type} ${response.access_token}`
          );

          runInAction(() => {
            store.tokenInfo = response;
            store.state = StatusTypes.DONE;
          });

          history.replace('/');
        })
        .catch((error: any) => {
          runInAction(() => {
            store.state = StatusTypes.ERROR
          });
          if (error.status === 401) {
            history.replace('/login');
          }
        });
    },
  }));

  return store;
};
