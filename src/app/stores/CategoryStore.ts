import { useLocalStore } from 'mobx-react';
import { runInAction } from 'mobx';
import services from '../services';
import { StatusTypes } from 'app/constants';

export type CategoryStore = ReturnType<typeof useCategoryStore>;

export const useCategoryStore = (history) => {
  const store = useLocalStore(() => ({
    categories: [],
    state: StatusTypes.PENDING,

    get activeCategories() {
      return store.categories;
    },

    get currentStatus() {
      return store.state;
    },

    getCategories() {
      const token = localStorage.getItem('token');
      services
        .SpotifyService()
        .getCategories(token)
        .then((response: any) => {
          runInAction(() => {
            store.categories = response.categories.items;
            store.state = StatusTypes.DONE;
          });
        })
        .catch((error: any) => {
          runInAction(() => {
            store.state = StatusTypes.ERROR;
          });
          if (error.response.status === 401) {
            history.replace('/login');
          }
        });
    },
  }));
  return store;
};
