import { useLocalStore } from 'mobx-react';
import { runInAction } from 'mobx';
import { StatusTypes } from 'app/constants';
import services from '../services';

export type PlaylistStore = ReturnType<typeof usePlaylistStore>;

export const usePlaylistStore = (history: any) => {
  const store = useLocalStore(() => ({
    playlists: [],
    state: StatusTypes.PENDING,

    get activePlaylists() {
      return store.playlists;
    },

    get currentStatus() {
      return store.state;
    },

    getPlaylists(categoryId: string) {
      const token = localStorage.getItem('token');
      services
        .SpotifyService()
        .getPlaylists(token, categoryId)
        .then((response: any) => {
          runInAction(() => {
            store.playlists = response.playlists.items;
            store.state = StatusTypes.DONE;
          });
        })
        .catch((error: any) => {
          runInAction(() => {
            store.state = StatusTypes.ERROR;
          });
          if (error.response.status === 401) {
            history.replace('/login');
          }
        });
    },
  }));
  return store;
};
